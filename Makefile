obj-m += syna-rmi4-minimal.o

OUT_DIR := .output

all:
	make -C $(KERN_DIR) M=$(PWD) O=$(OUT_DIR) modules

clean:
	make -C $(KERN_DIR) M=$(PWD) O=$(OUT_DIR) clean

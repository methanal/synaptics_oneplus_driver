# Downstream driver for OnePlus 6/6T Synaptics touchscreens.

Modified version of the [original driver](https://github.com/OnePlusOSS/android_kernel_oneplus_sdm845/blob/oneplus/SDM845_R_11.0/drivers/input/touchscreen/synaptics_driver_s3320.c) in the OnePlus downstream kernel.


## Kernel Changes

The DTS needs to be patched for the driver to function. Apply the diff provided (see `dts_patch.diff`) to the kernel tree.
Compile the kernel and install it to your device.


## Compiling the Kernel Module

In order to compile the kernel module, run:
```
KERN_DIR="/path/to/kernel/directory" make
```

The module should get compiled (see `synaptics_oneplus_driver.ko`). 


## Installing the Kernel Module

Copy the module (see `synaptics_oneplus_driver.ko`) to your device, and run:
```
sudo insmod synaptics_oneplus_driver.ko
```

The touchscreen should start working.


## Debugging

Logs from `dmesg` are very important for debugging. In order to get them, run:
```
sudo dmesg | grep "Synaptics OnePlus" 
```

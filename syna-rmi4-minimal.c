#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/input.h>
#include <linux/input/mt.h>
#include <linux/irq.h>
#include <linux/kernel.h>
#include <linux/of_gpio.h>
#include <linux/regulator/consumer.h>

#define SYNA_DRVNAME "Synaptics RMI4 (Minimal)"

#define SYNA_FUNC_ENTER pr_info("(ENTER) %s\n", __func__)
#define SYNA_FUNC_LEAVE pr_info("(LEAVE) %s\n", __func__)

#undef pr_fmt
#define pr_fmt(fmt) "rmi4-minimal: " fmt

struct syna_drvdata {
	struct i2c_client *client;
	struct input_dev *input;
	struct regulator *vdd;
	struct regulator *vio;
	struct mutex mutex;
	u32 max_x;
	u32 max_y;
	u8 f01_cmnd;
	u8 f01_ctrl;
	u8 f01_data;
	u8 f12_data;
	u8 f12_2d_input;
};

struct synop_poll_point {
	int x;
	int y;
	int z;
	int raw_x;
	int raw_y;
	unsigned char status;
};

static int syna_i2c_rx_block(struct syna_drvdata *drvdata, u8 addr, u16 len,
			     u8 *data)
{
	struct device *dev = &drvdata->client->dev;
	int ret;

	struct i2c_msg msg[] = {
		{
			.addr = drvdata->client->addr,
			.flags = 0,
			.len = 1,
			.buf = &addr,
		},
		{
			.addr = drvdata->client->addr,
			.flags = I2C_M_RD,
			.len = len,
			.buf = data,
		},
	};

	addr &= 0xff;

	ret = i2c_transfer(drvdata->client->adapter, msg, 2) != 2;
	if (ret) {
		dev_err(dev, "Failed to RX %u bytes from %#04x", len, addr);
		return ret;
	}

	dev_info(dev, "RX %u bytes at %#06x: (%*ph)\n", len, addr, len, data);

	return 0;
}

static int syna_i2c_tx_block(struct syna_drvdata *drvdata, u8 addr, u16 len,
			     const u8 *data)
{
	struct device *dev = &drvdata->client->dev;
	u8 *buf = kzalloc(len + 1, GFP_KERNEL);
	int ret = 0;

	struct i2c_msg msg[] = {
		{
			.addr = drvdata->client->addr,
			.flags = 0,
			.len = len + 1,
			.buf = buf,
		}
	};

	buf[0] = addr & 0xff;
	memcpy(buf + 1, data, len);

	ret = i2c_transfer(drvdata->client->adapter, msg, 1) != 1;
	if (ret) {
		dev_err(dev, "Failed to TX %u bytes from %#04x", len, buf[0]);
		goto end;
	}

	dev_info(dev, "TX %d bytes at %#06x: (%*ph)\n", len, addr, len, data);

end:
	kfree(buf);
	return ret;
}

static int syna_i2c_rx_byte(struct syna_drvdata *drvdata, u8 addr, u8 *data)
{
	return syna_i2c_rx_block(drvdata, addr, 1, data);
}

static int syna_i2c_tx_byte(struct syna_drvdata *drvdata, u8 addr, const u8 data)
{
	return syna_i2c_tx_block(drvdata, addr, 1, &data);
}

static int syna_cmnd_power_on(struct syna_drvdata *drvdata)
{
	struct device *dev = &drvdata->client->dev;
	int ret = 0;
	SYNA_FUNC_ENTER;

	if (regulator_enable(drvdata->vdd)) {
		dev_err(dev, "Failed to enable regulator `vdd` (%d)\n", ret);
		goto end;
	}

	usleep_range(10 * 1000, 10 * 1000);

	if (regulator_enable(drvdata->vio)) {
		dev_err(dev, "Failed to enable regulator `vio` (%d)\n", ret);
		goto end;
	}

	usleep_range(10 * 1000, 10 * 1000);

end:
	msleep(100);
	SYNA_FUNC_LEAVE;
	return ret;
}

static void syna_cmnd_power_off(struct syna_drvdata *drvdata)
{
	SYNA_FUNC_ENTER;

	regulator_disable(drvdata->vdd);
	regulator_disable(drvdata->vio);

	SYNA_FUNC_LEAVE;
}

static int syna_touch_reset(struct syna_drvdata *drvdata)
{
	struct device *dev = &drvdata->client->dev;
	int ret = 0;
	SYNA_FUNC_ENTER;

	ret = syna_i2c_tx_byte(drvdata, drvdata->f01_cmnd, 0x01);
	if (ret)
		dev_err(dev, "Reset command failed (%d)\n", ret);

	msleep(100);

	SYNA_FUNC_LEAVE;
	return ret;
}

static int syna_touch_config(struct syna_drvdata *drvdata)
{
	struct device *dev = &drvdata->client->dev;
	u8 dev_sts, mode;
	int ret = 0;
	SYNA_FUNC_ENTER;

	/* read the device status register */
	syna_i2c_rx_byte(drvdata, drvdata->f01_data, &dev_sts);

	if (dev_sts & BIT(7)) {
		ret = syna_i2c_rx_byte(drvdata, drvdata->f01_ctrl, &mode);
		if (ret)
			dev_err(dev, "Failed to get touchscreen mode\n");

		/* disable sleep mode */
		mode &= ~GENMASK(2, 0);
		/* what does this do? */
		mode &= ~BIT(5);
		/* set configured bit */
		mode |= BIT(7);

		ret = syna_i2c_tx_byte(drvdata, drvdata->f01_ctrl, mode);
		if (ret)
			dev_err(dev, "Failed to set touchscreen mode\n");
	}

	return ret;
}

static int syna_read_dts_props(struct syna_drvdata *drvdata)
{
	struct device *dev = &drvdata->client->dev;
	struct device_node *f12_node;
	int ret = 0;
	SYNA_FUNC_ENTER;

	drvdata->vdd = devm_regulator_get(dev, "vdd");
	if (IS_ERR(drvdata->vdd)) {
		ret = PTR_ERR(drvdata->vdd);
		dev_err(dev, "Failed to get regulator `vdd` (%d)\n", ret);
		goto end;
	}

	drvdata->vio = devm_regulator_get(dev, "vio");
	if (IS_ERR(drvdata->vio)) {
		ret = PTR_ERR(drvdata->vdd);
		dev_err(dev, "Failed to get regulator `vio` (%d)\n", ret);
		goto end;
	}

	f12_node = of_get_child_by_name(dev->of_node, "rmi4-f12");
	if (IS_ERR(f12_node)) {
		ret = PTR_ERR(f12_node);
		dev_err(dev, "Failed to find F12 subnode (%d)\n", ret);
		goto of_put;
	}

	ret = of_property_read_u32(f12_node, "syna,clip-x-high", &drvdata->max_x);
	if (ret) {
		dev_err(dev, "DT property `syna,clip-x-high` not found (%d)\n", ret);
		goto of_put;
	}

	ret = of_property_read_u32(f12_node, "syna,clip-y-high", &drvdata->max_y);
	if (ret) {
		dev_err(dev, "DT property `syna,clip-y-high` not found (%d)\n", ret);
		goto of_put;
	}

of_put:
	of_node_put(f12_node);
end:
	SYNA_FUNC_LEAVE;
	return ret;
}

static int syna_read_register_addresses(struct syna_drvdata *drvdata)
{
	u8 buf[4];
	int ret = 0;
	SYNA_FUNC_ENTER;

	/*
	 * Read the Function Descriptor registers.
	 *
	 * Each descriptor spans 6 bytes, but 4 bytes are read here.
	 * Some OnePlus aftermarket touchscreens do not report the
	 * descriptor registers properly if 6 bytes are read.
	 *
	 * Ref: RMI4 Manual (PN: 511-000136-01 Rev. E), Section 2.3.5.2
	 */

	/* PDT: 0xe3 to 0xe8 */
	ret |= syna_i2c_rx_block(drvdata, 0xe3, 4, buf);

	drvdata->f01_cmnd = buf[1];
	drvdata->f01_ctrl = buf[2];
	drvdata->f01_data = buf[3];

	/* PDT: 0xdd to 0xe2 */
	ret |= syna_i2c_rx_block(drvdata, 0xdd, 4, buf);

	drvdata->f12_2d_input = 0x0B;
	drvdata->f12_data = buf[3];

	SYNA_FUNC_LEAVE;
	return ret;
}

// TODO: Cleanup
static void synop_poll_touch(struct syna_drvdata *drvdata)
{
	struct device *dev = &drvdata->client->dev;
	struct synop_poll_point point;
	u8 buf[90] = {0};
	u8 object_attention[2];
	u8 count_data = 0;
	u16 total_status = 0;
	u32 finger_info = 0;
	u8 finger_status = 0;
	u8 finger_num = 0;
	int i = 0;

	SYNA_FUNC_ENTER;

	syna_i2c_rx_block(drvdata, drvdata->f12_2d_input, 2, object_attention);

	total_status = (object_attention[1] << 8) | object_attention[0];

	while (total_status) {
		count_data++;
		total_status >>= 1;
        }

	if (count_data > 10) {
		dev_err(dev, "Abnormal finger count: %d\n", count_data);
		// goto end;
	}

	/* temp: force count_data to 1 */
	count_data = 1;

	syna_i2c_rx_block(drvdata, drvdata->f12_data, count_data * 8 + 1, buf);

	for (i = 0; i < count_data; i ++) {
		point.x = ((buf[i * 8 + 2] & 0x0f) << 8) | (buf[i * 8 + 1] & 0xff);
		point.y = ((buf[i * 8 + 4] & 0x0f) << 8) | (buf[i * 8 + 3] & 0xff);

		point.raw_x = buf[i * 8 + 6] & 0x0f;
		point.raw_y = buf[i * 8 + 7] & 0x0f;

		point.status = buf[i * 8];

		finger_info <<= 1;
		finger_status = point.status & 0x03;

		if (finger_status) {
			input_mt_slot(drvdata->input, i);
			input_mt_report_slot_state(drvdata->input, MT_TOOL_FINGER, finger_status);

			input_report_abs(drvdata->input, ABS_MT_POSITION_X, point.x);
			input_report_abs(drvdata->input, ABS_MT_POSITION_Y, point.y);

			input_report_abs(drvdata->input, ABS_MT_TOUCH_MAJOR, max(point.raw_x, point.raw_y));
			input_report_abs(drvdata->input, ABS_MT_TOUCH_MINOR, min(point.raw_x, point.raw_y));

			input_report_key(drvdata->input, BTN_TOUCH, 1);

			/* These events will be based on the first finger in queue. */
			if (i == 0) {
				input_report_abs(drvdata->input, ABS_X, point.x);
				input_report_abs(drvdata->input, ABS_Y, point.y);
			}

			finger_num++;
			finger_info |= 1;
		}
	}

	finger_info <<= (10 - count_data);

	for (i = 0; i < 10; i ++) {
		finger_status = (finger_info >> (10 - i - 1)) & 1;

		if (!finger_status) {
			input_mt_slot(drvdata->input, i);
			input_mt_report_slot_state(drvdata->input, MT_TOOL_FINGER, finger_status);
		}
	}

	if (finger_num == 0)
		input_report_key(drvdata->input, BTN_TOUCH, 0);

	input_sync(drvdata->input);

end:
	SYNA_FUNC_LEAVE;
}

static irqreturn_t syna_poll_irq(int irq, void *data)
{
	struct syna_drvdata *drvdata = data;
	u8 int_sts;
	int ret;
	SYNA_FUNC_ENTER;

	mutex_lock(&drvdata->mutex);

	/* read the interrupt status register */
	syna_i2c_rx_byte(drvdata, drvdata->f01_data + 1, &int_sts);

	if (int_sts & BIT(2))
		synop_poll_touch(drvdata);

	mutex_unlock(&drvdata->mutex);

	SYNA_FUNC_LEAVE;
	return IRQ_HANDLED;
}

static int syna_input_init(struct syna_drvdata *drvdata)
{
	struct device *dev = &drvdata->client->dev;
	int ret = 0;

	SYNA_FUNC_ENTER;

	drvdata->input = devm_input_allocate_device(dev);
	if (!drvdata->input) {
		ret = -ENOMEM;
		dev_err(dev, "Failed to allocate input device\n");
		goto end;
	}

	drvdata->input->name = SYNA_DRVNAME;
	drvdata->input->dev.parent = &drvdata->client->dev;

	input_set_drvdata(drvdata->input, drvdata);
	input_mt_init_slots(drvdata->input, 10, 0);

	set_bit(INPUT_PROP_DIRECT, drvdata->input->propbit);
	set_bit(BTN_TOUCH, drvdata->input->keybit);
	set_bit(EV_SYN, drvdata->input->evbit);
	set_bit(EV_ABS, drvdata->input->evbit);
	set_bit(EV_KEY, drvdata->input->evbit);

	set_bit(ABS_MT_PRESSURE, drvdata->input->absbit);
	set_bit(ABS_MT_POSITION_X, drvdata->input->absbit);
	set_bit(ABS_MT_POSITION_Y, drvdata->input->absbit);
	set_bit(ABS_MT_TOUCH_MAJOR, drvdata->input->absbit);
	set_bit(ABS_X, drvdata->input->absbit);
	set_bit(ABS_Y, drvdata->input->absbit);

	input_set_abs_params(drvdata->input, ABS_MT_PRESSURE, 0, 255, 0, 0);
	input_set_abs_params(drvdata->input, ABS_MT_POSITION_X, 0, drvdata->max_x, 0, 0);
	input_set_abs_params(drvdata->input, ABS_MT_POSITION_Y, 0, drvdata->max_y, 0, 0);
	input_set_abs_params(drvdata->input, ABS_MT_TOUCH_MAJOR, 0, 255, 0, 0);
	input_set_abs_params(drvdata->input, ABS_MT_TOUCH_MINOR, 0, 255, 0, 0);
	input_set_abs_params(drvdata->input, ABS_X, 0, drvdata->max_x, 0, 0);
	input_set_abs_params(drvdata->input, ABS_Y, 0, drvdata->max_y, 0, 0);

	ret = input_register_device(drvdata->input);
	if (ret) {
		dev_err(dev, "Failed to register input device\n");
		input_unregister_device(drvdata->input);
	}

end: 
	SYNA_FUNC_LEAVE;
	return ret;
}

static int syna_probe(struct i2c_client *client)
{
	struct syna_drvdata *drvdata;
	struct device *dev = &client->dev;
	int ret = 0;
	SYNA_FUNC_ENTER;

	drvdata = devm_kzalloc(&client->dev, sizeof(*drvdata), GFP_KERNEL);
	if (!drvdata) {
		ret = -ENOMEM;
		goto end;
	}

	drvdata->client = client;
	i2c_set_clientdata(client, drvdata);

	ret = syna_read_dts_props(drvdata);
	if (ret)
		dev_err_probe(dev, ret, "Failed to read DT properties\n");

	mutex_init(&drvdata->mutex);
	ret = syna_cmnd_power_on(drvdata);
	if (ret)
		dev_err_probe(dev, ret, "Failed to power on the device\n");

	/* explicitly set the page to 0 */
	syna_i2c_tx_byte(drvdata, 0xff, 0x0);

	ret = syna_read_register_addresses(drvdata);
	if (ret)
		dev_err_probe(dev, ret, "Failed to read vital register addresses\n");

	ret = syna_touch_reset(drvdata);
	if (ret)
		dev_err_probe(dev, ret, "Failed to reset the touchscreen\n");

	ret = syna_touch_config(drvdata);
	if (ret)
		dev_err_probe(dev, ret, "Failed to configure the touchscreen\n");

	ret = syna_input_init(drvdata);
	if (ret)
		dev_err_probe(dev, ret, "Failed to initialize input device\n");

	return devm_request_threaded_irq(dev, client->irq, NULL, syna_poll_irq,
					 irq_get_trigger_type(client->irq) | IRQF_ONESHOT,
					 SYNA_DRVNAME, drvdata);

end:
	SYNA_FUNC_LEAVE;
	return ret;
}

static void syna_remove(struct i2c_client *client)
{
	struct syna_drvdata *drvdata = i2c_get_clientdata(client); 
	SYNA_FUNC_ENTER;

	syna_cmnd_power_off(drvdata);
	mutex_destroy(&drvdata->mutex);

	SYNA_FUNC_LEAVE;
}

static struct of_device_id syna_compatible[] = {
	/* same compatible string as the mainline rmi4 driver */
	{ .compatible = "syna,rmi4-i2c" },
	{ },
};

static struct i2c_driver syna_driver = {
	.probe = syna_probe,
	.remove = syna_remove,
	.driver = {
		.name = SYNA_DRVNAME,
		.of_match_table = syna_compatible,
	},
};

module_i2c_driver(syna_driver);

MODULE_DESCRIPTION("Synaptics RMI4 Driver (Minimal)");
MODULE_LICENSE("GPL");
